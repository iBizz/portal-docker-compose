#!/bin/bash

rm -rf core_profile
rm -rf dam_postgres
rm -rf dam_assets

rm nginx/nginx.key
rm nginx/nginx.crt
rm nginx/dhparam.pem

