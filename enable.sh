#!/bin/bash

docker-compose exec portal mkdir -p /opt/HCL/wp_profile/cloud
docker-compose exec portal /opt/HCL/ConfigEngine/ConfigEngine.sh action-op-deploy-extension-products -DPortalAdminId=wpsadmin -DPortalAdminPwd=wpsadmin -DWasUserid=wpsadmin -DWasPassword=wpsadmin -Dmedialibrary=true -Dheadlesscontent=true -DconfigureLdap=false -Dcc.static.ui.url=https://localhost/dx/ui/content/static -Ddam.static.ui.url=https://localhost/dx/ui/dam/static -DdigitalAssets.useSSLDAM=true
