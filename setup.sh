#!/bin/bash

mkdir -p core_profile
mkdir -p dam_postgres
mkdir -p dam_assets

chown 1000:1001 core_profile
chown 1000:1001 dam_postgres
chown 1000:1001 dam_assets

if [ ! -f nginx/nginx.crt ]; then
	openssl req -x509 -nodes -days 3650 -newkey rsa:2048 -keyout nginx/nginx.key -out nginx/nginx.crt -subj '/CN=local-dx'
fi

if [ ! -f nginx/dhparam.pem ]; then
	openssl dhparam -out nginx/dhparam.pem 2048
fi
